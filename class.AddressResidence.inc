<?php

/**
 * Állandó cím bővítés
 */
class AddressResidence extends Address
{

    //  protected $_address_type_id = 1;//property redeclare

    //kiírás felülírása -> hozáadunk egy taget meg némi classt
    public function display()
    {
        $ret = '<address class="residence alert alert-info col-4">';
        $ret .= parent::display();
        $ret .= '</address>';

        return $ret;
    }

    protected function _init()
    {
        //die('init');
        $this->_setAddressTypeId(self::ADDRESS_TYPE_RESIDENCE);
    }
}