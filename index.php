<!doctype html>
<html lang="hu">
 <head>
  <title>PHP haladó  tanfolyam - Ruander Oktatóközpont</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!--saját stílusok-->
	<link rel="stylesheet" href="custom.css">
 </head>
 <body>
  <div class="container">
    <div class="row">
     <header class="col page-header">
      <h1 class="text-center">PHP haladó  tanfolyam - Ruander Oktatóközpont</h1>
     </header> 
    </div>
	<div class="row content">

	  <?php include "test.php"; ?>

	</div>
  <footer class="text-center">Ruander &copy; <?php echo date('Y. F d.');?></footer>
  </div>
 </body>
</html>