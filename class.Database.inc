<?php
/**
 * Created by PhpStorm.
 * User: hgy
 * Date: 2018. 03. 25.
 * Time: 11:04
 * Adatbázis egyke (singleton)
 */
class Database {
    //kapcsolat
    private $_connection;
    //példány
    private static $_instance;

    //példány kiadása
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self;
        }
        return self::$_instance;
    }

    public function __construct()
    {
        $this->_connection = new mysqli('localhost','root','','hgy_oop');
    //hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Nem sikerült az adatbázis kapcsolat: '.mysqli_connect_error(),E_USER_ERROR);
        }

    }
    //védett kapcsolat kiadása a külső osztályok számára
    public function getConnection(){
        return $this->_connection;
    }

    //klónozás elleni védelem
    final public function __clone(){}
}