<?php
/**
 * Custom kivételkezelés bővítés a beépített Exception classra
 */
class ExceptionAddress extends Exception {

    public function __toString()
    {
        return $this->getMessage()."[{$this->getCode()}]";
    }

}