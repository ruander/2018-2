<?php
/**
 * Interface Model
 * interfész az interakcióknak
 */

interface Model
{
    //egy cím betöltés
    public static function load($id);

    //cím eltárolása
    public function save();

    //cím módsítása
    public function update();

    //cím törlése
    public function delete();

    //egyéb interakciók

    //összes cím lekérése
    public static function all();

    public static function getInstance($data);
}