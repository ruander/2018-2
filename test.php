<?php
//autoload az osztályok betöltésére
function __autoload($name)
{
    $classname = "class.$name.inc";
    include $classname;
}

echo '<h2 class="col-12">Osztály bővítés bemutatása (állandó lakcím bővítés)</h2>';


$data = [
    'street_address_1' => 'Valami tér 25.',
    'street_address_2' => 'IV. emelet 24.',
    'city_name' => 'Szentendre',
    //'postal_code' => 2333,
    'country_name' => 'Magyarország',
];
$address_residence = new AddressResidence($data);
//echo '<pre>' . var_export($address_residence, true) . '</pre>';
echo $address_residence->save();
echo '<pre>' . var_export($address_residence, true) . '</pre>';
echo $address_residence;

//getinstance test
try {//ha itt keletkezik hiba a catch blokk fut le
    $test_object = Address::getInstance();
    echo $test_object->display();
    echo '<pre>' . var_export($test_object, true) . '</pre>';
} catch (ExceptionAddress $e) {//megkapjuk $e ben a hibát
    echo $e;
}


//cím betöltés
echo '<h2 class="col-12">Objektum betöltése adatbázisból</h2>';
try {
    $address_db = Address::load(0);
    echo $address_db->display();
    echo '<pre>' . var_export($address_db, true) . '</pre>';
}
catch(ExceptionAddress $e){
    echo $e;
}
$addresses = Address::all();
//lista CRUD (new|mod|del)
