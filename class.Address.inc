<?php
/**
 * Címkezelő osztály
 */

abstract class Address implements Model
{
    //Osztályállandók
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSINESS = 2;
    const ADDRESS_TYPE_TEMPORARY = 3;

    //Egyedi hibakódok az exceptionok kódjaihoz
    const ADDRESS_ERROR_NOT_FOUND = 1000;
    const ADDRESS_ERROR_INVALID_ADDRESS_TYPE = 1001;
    //Érvényes címtipus azonosítók + kiírható nevek
    public static $valid_address_types = [
        self::ADDRESS_TYPE_RESIDENCE => 'Residence',
        self::ADDRESS_TYPE_BUSINESS => 'Business',
        Address::ADDRESS_TYPE_TEMPORARY => 'Temporary',
    ];

    //címsor 1
    public $street_address_1;
    //címsor 2
    public $street_address_2;
    //irsz
    protected $_postal_code;
    //város
    public $city_name;
    //város rész
    public $subdivision_name;
    //ország
    public $country_name;

    //cím azonosító
    protected $_address_id;
    //cím tipus azonosító
    protected $_address_type_id;
    //timestamps
    protected $_time_created = 12345;
    protected $_time_updated = 67890;

    /**
     * Address constructor. (new futtatja)
     * @param array $data - optional
     */
    public function __construct($data = [])
    {
        $this->_time_created = time();
        $this->_init();//minden ext nek tartalmaznia kell ˇˇˇˇˇ abstract init
        //objektum felépítése a kapott adatokból, ha az tömb
        if (is_array($data)) {

            foreach ($data as $name => $value) {
                if (in_array($name, ['address_id', 'time_created', 'time_updated','address_type_id'])) {//ha védett tulajdonságról van szó
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        } else {
            trigger_error('Nem lehet felépíteni objektumot a kapott adatokból!');
        }
    }

    /**
     * Magic __Get, akkor fut ha nem létező vagy védett tulajdonságot próbálunk elérni (kiolvasni)
     * @param string $name
     */
    public function __get($name)
    {
        //var_dump('fut a get, emiatt:' . $name);
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }

        $protected_property_name = '_' . $name;
        //ha létezik ilyen védett tulajdonság, adjuk vissza azt
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }
        //ha nincs ilyen -> trigger error
        trigger_error("Nem létező vagy védett tulajdonságot próbáltunk elérni (__get): $name ");
    }

    /**
     * Magic __set, akkor fut amikor nem létező vagy védett tulajdonságot próbálunk beállítani
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        //var_dump('fut a set, emiatt:' . $name . '|' . $value);
        if ($name == "postal_code") {
            $this->$name = $value;
            return;
        }

        trigger_error("Nem létező vagy védett tulajdonságot próbáltunk beállítani (__set): $name -> $value");
    }

    /**
     * Objektum kiírása (echo $object esetében fut, amikor stringgé kéne konvertálni az objektumot, amit nem lehet
     * @return string
     */
    public function __toString()
    {
        return $this->display();
    }

    //_init eljárás megkövetelése a bővítésektől
    abstract protected function _init();

    /**
     * Irányítószám keresés teljes városnév és városrész név alapján.
     * @return string
     */
    protected function _postal_code_search()
    {
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $qry = "SELECT irsz 
                  FROM telepulesek 
                  WHERE telepules = '$city_name' 
                  AND telepules_resz= '$subdivision_name' ";
        $result = $mysqli->query($qry) or die($mysqli->error);
        //var_dump($result->fetch_object());
        if ($row = $result->fetch_row()) {
            return $row[0];
        }
        return 'Nem találtam';
    }

    /**
     * Irányítószám apinak, szövegrészlet alapján ad vissza többdimenziós tömböt a találatokkal (json)
     * [irsz],[telepulesnev],[telepules_resz]
     * @param string $q //the search string
     * @return json|string (array)
     */
    public static function postal_code_api_search($q = '')
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $q = $mysqli->real_escape_string($q);
        $qry = "SELECT *
                  FROM telepulesek 
                  WHERE telepules LIKE '%$q%' 
                  OR telepules_resz LIKE '%$q%'  ";
        $result = $mysqli->query($qry) or die($mysqli->error);
        //var_dump($result->fetch_object());
        $rows = $result->fetch_all(true);

        return json_encode($rows);
    }

    /**
     * Cím példány kiírása
     * @return string
     */
    public function display()
    {
        $ret = $this->street_address_1;
        if ($this->street_address_2) {
            $ret .= "<br>{$this->street_address_2}";
        }
        $ret .= "<br>$this->postal_code, $this->city_name";
        if ($this->subdivision_name) {
            $ret .= ", $this->subdivision_name";
        }
        $ret .= "<br>$this->country_name";
        return $ret;
    }

    /**
     * A kapott paraméter alapján tér vissza hogy érvényes e (benne van a $valid_address_types tömbben a kulcsa
     * @param $address_type_id
     * @return bool
     */
    public static function isValidAddressTypeId($address_type_id)
    {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * address type id beállítása
     * @param  int $address_type_id
     * @return void
     */
    protected function _setAddressTypeId($address_type_id)
    {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
        return;
    }

    final public static function load($id)
    {
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $qry = "SELECT * 
                  FROM addresses
                  WHERE address_id = ".(int) $id." 
                  LIMIT 1 ";
        $result = $mysqli->query($qry) or die($mysqli->error);
        if($row = $result->fetch_assoc()) {
            //példányosítás
            $ret = self::getInstance($row['address_type_id'],$row);
            return $ret;
            //die(var_export($row,true));
        }
        throw new ExceptionAddress('Address Error - Address not found! ',self::ADDRESS_ERROR_NOT_FOUND);
    }

    /**
     * elem mentése
     * @return inserted element id
     */
    public function save()
    {
        if($this->_address_id) return $this->update();//ha van id akkor már volt mentve, akkor meg inkább update kell, ne duplikálgassunk :)
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $street_address_1 = $mysqli->real_escape_string($this->street_address_1);
        $street_address_2 = $mysqli->real_escape_string($this->street_address_2);
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $country_name = $mysqli->real_escape_string($this->country_name);
        echo $qry = "INSERT INTO `addresses` (
                        `address_type_id`, 
                        `street_address_1`, 
                        `street_address_2`, 
                        `postal_code`, 
                        `subdivision_name`, 
                        `city_name`, 
                        `country_name`, 
                        `time_created` 
                        ) VALUES (
                       '$this->_address_type_id', 
                       '$street_address_1', 
                       '$street_address_2', 
                       '$this->postal_code', 
                       '$subdivision_name', 
                       '$city_name', 
                       '$country_name', 
                       '" . date('Y-m-d H:i:s', $this->_time_created) . "')";

        $mysqli->query($qry) or die($mysqli->error);
        $id = $mysqli->insert_id;
        //az objektumba is beírjuk a kapott id-t
        if(is_int($id)) {
            $this->_address_id = $id;
            return $id;
        }
        return false;
    }

    public function update()
    {
        if(!$this->_address_id) return $this->save();//ha nincs id akkor inkább save kell :)
        //csatlakozás DBhez
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        //kódtábla illesztés az ékezetes karakterek miatt
        $mysqli->set_charset("utf8");
        $street_address_1 = $mysqli->real_escape_string($this->street_address_1);
        $street_address_2 = $mysqli->real_escape_string($this->street_address_2);
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision_name = $mysqli->real_escape_string($this->subdivision_name);
        $country_name = $mysqli->real_escape_string($this->country_name);
        $time_updated = date('Y-m-d H:i:s');
        $qry = "UPDATE addresses 
         SET 
         `address_type_id` = $this->_address_type_id, 
         `street_address_1` = '$street_address_1', 
         `street_address_2` = '$street_address_2', 
         `postal_code` = '$this->postal_code', 
         `subdivision_name` = '$this->subdivision_name', 
         `city_name` = '$this->city_name', 
         `country_name` = '$this->country_name',
         `time_updated` = '$time_updated'
         WHERE `address_id` = $this->_address_id
         ";
        $mysqli->query($qry) or die($mysqli->error);

        return $this->_address_id;
    }

    /**
     * elem törlése
     * @return destroyed element id
     */
    public function delete()
    {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        echo $qry = "DELETE FROM addresses WHERE address_id =  $this->_address_id LIMIT 1 ";
        $mysqli->query($qry) or die($mysqli->error);
        return $this->_address_id;
    }
     //cím példányosítás asszociatív tömbből
    final public static function getInstance($address_type_id = self::ADDRESS_TYPE_RESIDENCE,$data = [] ){

        if(self::isValidAddressTypeId($address_type_id)){
            $classname = 'Address' . self::$valid_address_types[$address_type_id];
            $ret = new $classname($data);
            return $ret;
        }

        throw new ExceptionAddress("Address Error! Can't instantiate Address! ",Address::ADDRESS_ERROR_INVALID_ADDRESS_TYPE);

    }

    //összes cím lekérése

    /**
     * HF
     * @return $tomb[n] => $object
     */
    public static function all(){}

}