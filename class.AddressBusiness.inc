<?php
/**
 * Számlázási cím bővítés
 */
class AddressBusiness extends Address {

    //kiírás felülírása -> hozáadunk egy taget meg némi classt
    public function display()
    {
        $ret = '<address class="business alert alert-warning col-4">';
        $ret .= parent::display();
        $ret .= '</address>';

        return $ret;
    }

   //address type id inícializálása
    protected function _init(){
        $this->_setAddressTypeId(self::ADDRESS_TYPE_BUSINESS);
    }
}